import bisect
import math
import numpy as np
import pandas as pd


class Vortex:
    effects_list = [
        [-0.880240198450349, -0.405916780118671, 0.196665917244385, 0.636391799085122,
         1.02647645411015],  # x0

        [-0.616854647083324, -0.26519261679275, 0.146803706485113, 0.35377765867972,
         0.545623046570569],  # x1

        [-0.199699869707061, -0.0643630695080423, 0.0939862995505662, 0.288115833035428,
         0.438113372429146],  # x2

        [0.159539880017455, 0.0932125626926394, -0.0108282855458462, -0.244760946110628,
         -0.665693380069762],  # x3

        [0.397417475553939, 0.217470745176491, -0.0858511760771992, -0.257985074234951,
         -0.678935786125728],  # x4

        [0.183084826832122, -0.141086865400225, -0.43925202549218],  # x5

        [-0.0208046912362649, 0.673863701171483],  # x6

        [-0.390942813156511, -0.149031837031994, 0.13433778745637, 0.529418697752807]  # x7
    ]

    weights = [
        0.4761476,  # x0
        0.3208511,  # x1
        0.2064462,  # x2
        0.2587311,  # x3
        0.3342483,  # x4
        0.3187723,  # x5
        0.2848142,  # x6
        0.4116576   # x7
    ]

    woe_list = [
        [-1.848670871070964, -0.8525019975290666, 0.41303561594006777, 1.336543120421319,
         2.155794661382626],  # x0

        [-1.9225573703294894, -0.8265286196392969, 0.45754465696116675, 1.1026225519554709,
         1.7005490913715708],  # x1

        [-0.9673216058569303, -0.31176679206515934, 0.4552580747457023, 1.3955976570914261,
         2.1221672882772653],  # x2

        [0.6166242868269606, 0.36026810341949383, -0.0418515035333835, -0.946005123120599,
         -2.572915973648943],  # x3

        [1.1889887713832472, 0.6506263313126529, -0.2568485047708521, -0.77183660839846,
         -2.031231830126669],  # x4

        [0.5743435889257692, -0.4425944958210767, -1.3779491677670237],  # x5

        [-0.07304653783506897, 2.365976489836121],  # x6

        [-0.949679571460629, -0.36202863018196185, 0.32633379647641636, 1.286065647161153]  #x7
    ]

    bins = [
        [432, 868, 1253, 2490],  # x0
        [0, 22, 42, 96],         # x1
        [0, 355, 730, 1388],     # x2
        [25, 37, 60, 80],        # x3
        [175, 239, 639, 1071],   # x4
        [1801, 4501],            # x5
        [0],                     # x6
        [25, 57, 164]            # x7
    ]

    constant = -3.40922238280873

    def predict_one(self, instance):
        res = self.constant
        for e, val in enumerate(instance):
            res += self.effects_list[e][int(val)]
        return 1 - 1 / (1 + math.exp(res))

    def predict(self, instances):
        # if isinstance(instances, pd.DataFrame):
        #     instances = instances.to_numpy()
        # if len(instances.shape) == 1:
        #     return self.predict_one(instances)
        res = []
        for instance in instances:
            res.append(self.predict_one(instance))
        return np.array(res)

    def bin_column(self, column, index):
        if len(column.shape) == 0:
            return bisect.bisect_left(self.bins[index], column)
        res = []
        for value in column:
            res.append(bisect.bisect_left(self.bins[index], value))
        return res

    def predict_unbinned(self, instances):
        instances = np.array([self.bin_column(v, i) for i, v in enumerate(instances.T)]).T
        return self.predict(instances)

#
# if __name__ == '__main__':
#     x = np.array([[   0.,    0.,    0.,   24.,  650., 3251.,    0.,   10.]])
#     vortex = Vortex()
#     for i in range(7):
#         print(vortex.bins[i])
